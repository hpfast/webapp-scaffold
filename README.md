Template for a Webapp
=====================

Copy this template to get started with developing a webapp. It provides a folder structure and development/build tools so you can get live previews of your work and use NPM-style javascript and modules.


Installation
------------

    git clone https://gitlab.com/hpfast/webapp-scaffold
    cd webapp-scaffold
    npm install


Usage
-----

All working files are in `assets`. Edit `index.html` and `script/index.js` and `style/main.css`.

The following commands are available for generating your project and running development mode:

    npm run build   #build html, js, and css
    npm run dev     #build assets, start webserver, and watch assets for changes: recompiles and reloads webpage on changes.

Everything is put in `dist`. You can copy the contents of this directory to deploy your app.

A browser window is opened at `http://localhost:9090`.
